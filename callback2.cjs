
const boardsListsData = require('./lists.json')
/*Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it 
from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/


function callback2(boardID, callback) {
    if (typeof boardID !== 'string' || typeof callback !== 'function' || boardID == "") {
        const error = "INVALID ARGUMENTS"
        callback(error)
        return
    }

    const timeDelay = 2

    setTimeout(() => {

        if(boardsListsData[boardID]==undefined){
            const error = 'BOARDID NOT AVAILABLE IN THE LIST'
            callback(error)
        }else {
                callback(null, boardsListsData[boardID])
        }

    }, timeDelay * 1000)


}

module.exports = callback2