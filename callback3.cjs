
const cardsData = require('./cards.json')

/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is 
    passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

function callback3(listId, callback) {
    if (typeof listId !== 'string' || typeof callback !== 'function' || listId == "") {
        const error = "INVALID ARGUMENTS"
        callback(error)
        return
    }

    const timeDelay = 2

    setTimeout(() => {
        if (cardsData[listId] == undefined) {
            const error = 'LISTID NOT AVAILABLE IN CARDS.JSON'
            callback(error)
        } else {
            callback(null, cardsData[listId])
        }

    }, timeDelay * 1000)


}

module.exports = callback3