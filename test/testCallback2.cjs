
const path = require('path')
const callback2 = require(path.join(__dirname, '../callback2.cjs'))
const boardId = 'mcu453ed'

callback2(boardId, (error, data) => {
    if (error) {
        console.error(error)
    } else {
        console.log("SUCCESS:")
        console.log(data)
    }
})

