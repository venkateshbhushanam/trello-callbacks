const path = require('path')
const boardsInformation = require(path.join(__dirname, '/callback1.cjs'))
const boardsLists = require(path.join(__dirname, '/callback2.cjs'))
const cardsLists = require(path.join(__dirname, '/callback3.cjs'))

function callback6(thanosId = 'mcu453ed') {

    boardsInformation(thanosId, (error, data) => {
        if (error) {
            console.error(error)
        } else {
            console.log("SUCCESS:")
            console.log(data)
            
            boardsLists(thanosId, (error, data) => {
                if (error) {
                    console.error(error)
                } else {
                    console.log("SUCCESS:")
                    console.log(data)

                    for (let eachItem of data) {
                        cardsLists(eachItem.id, (error, data) => {
                            if (error) {
                                console.error(error)
                            } else {
                                console.log(`${eachItem.name} SUCCESS:`)
                                console.log(data)
                            }
                        })

                    }
                }
            })
        }

    })
}

module.exports = callback6
