const path = require('path')
const boardsInformation = require(path.join(__dirname, '/callback1.cjs'))
const boardsLists = require(path.join(__dirname, '/callback2.cjs'))
const cardsLists = require(path.join(__dirname, '/callback3.cjs'))

/* 
    Problem 4: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

function callback4(thanosId = 'mcu453ed', personName = 'Mind') {

    boardsInformation(thanosId, (error, data) => {
        if (error) {
            console.error(error)
        } else {
            console.log("SUCCESS:")
            console.log(data)
            boardsLists(thanosId, (error, data) => {
                if (error) {
                    console.error(error)
                } else {
                    console.log("SUCCESS:")
                    console.log(data)

                    let mindId = data.find(each => {
                        return each.name == personName
                    })

                    cardsLists(mindId.id, (error, data) => {
                        if (error) {
                            console.error(error)
                        } else {
                            console.log("SUCCESS:")
                            console.log(data)
                        }
                    })
                }
            })
        }

    })


}

module.exports = callback4

