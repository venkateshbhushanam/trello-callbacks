const path = require('path')
const boardsInformation = require(path.join(__dirname, '/callback1.cjs'))
const boardsLists = require(path.join(__dirname, '/callback2.cjs'))
const cardsLists = require(path.join(__dirname, '/callback3.cjs'))

/*
Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

Get information from the Thanos boards
Get all the lists for the Thanos board
Get all cards for the Mind and Space lists simultaneously
*/


function callback5(thanosId = 'mcu453ed') {

    boardsInformation(thanosId, (error, data) => {
        if (error) {
            console.error(error)
        } else {
            console.log("SUCCESS:")
            console.log(data)

            boardsLists(thanosId, (error, data) => {
                if (error) {
                    console.error(error)
                } else {
                    console.log("SUCCESS:")
                    console.log(data)

                    for (let eachItem of data) {
                        if (eachItem.name == 'Mind' || eachItem.name == "Space") {
                            cardsLists(eachItem.id, (error, data) => {
                                if (error) {
                                    console.error(error)
                                } else {
                                    console.log(`${eachItem.name} SUCCESS:`)
                                    console.log(data)
                                }
                            })

                        }

                    }

                }
            })
        }

    })

}

module.exports = callback5

