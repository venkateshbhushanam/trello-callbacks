
const bordsData = require('./boards.json')
/*Problem 1: Write a function that will return a particular board's information based on the boardID that is passed 
from the given list of boards in boards.json and then pass control back to the code that called it by using a callback 
function.*/

function callback1(boardID, callback) {
    if (typeof boardID !== 'string' || typeof callback !== 'function' || boardID == "") {
        const error = "INVALID ARGUMENTS"
        callback(error)
        return
    }
    
    const timeDelay = 2
    
    setTimeout(() => {
        const isAvailable = bordsData.find(eachBoard => {
            if (eachBoard.id == boardID) {
                return true
            }
        })
        if (isAvailable == undefined) {
            const error = 'BORDID NOT AVAILABLE'
            callback(error)
        } else {
            callback(null, isAvailable)
        }
        
    }, timeDelay * 1000)
    
}

module.exports = callback1